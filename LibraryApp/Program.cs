﻿using System;
using System.Collections.Generic;


namespace Library
{
    public class Book : ICloneable
    {
        private DateTime date = DateTime.MaxValue;

        public string Name { get; set; }
        public string Author { get; set; }
        public Subscriber Sub { get; private set; } = null;
        public bool Rarity { get; set; }
        public bool OverD { get; set; } = false;

        public string About_book => $"Название: {Name}, автор: {Author}, редкость: {Rarity}";

        public DateTime Date { get => date; set => date = value; }

        public Book(string name, string author, bool rarity)
        {
            this.Name = name;
            this.Author = author;
            this.Rarity = rarity;
        }

        public bool IsBookInLibrary() // Книга в библиотеке?
        {
            if (Sub == null)
                return true;
            else
                return false;
        }

        public bool IsOverdue(DateTime currentDate) // Книга просрочена?
        {
            return (currentDate - date).TotalDays > 14;
        }

        public void GetBookFromLibrary(Subscriber subscriber, DateTime currentDate) // Выдать книгу!
        {
            if (this.IsBookInLibrary() == false)
                throw new Exception($"{About_book} не может быть выдана");

            Sub = subscriber;
            date = currentDate;
        }

        public void ReturnBook() // Возвратить книгу!
        {
            if (this.IsBookInLibrary() == true)
                throw new Exception($"{About_book} уже находится в библиотеке");

            Sub = null;
            date = DateTime.MaxValue;
        }

        public DateTime WhenBook() // Где книга?
        {
            if (this.IsBookInLibrary() == false)
                return (this.date);
            else
                throw new Exception($"{About_book} находится в библиотеке");
        }

        public object Clone() // Копировать книгу!
        {
            if (this.Sub?.Name != null)
            {
                Subscriber man = new Subscriber(this.Sub.Name)
                { Name = this.Sub.Name };
                return new Book(this.Name, this.Author, this.Rarity)
                {
                    Name = this.Name,
                    Author = this.Author,
                    Rarity = this.Rarity,
                    OverD = this.OverD,
                    Date = this.Date,
                    Sub = man
                };
            }
            else
            {
                return new Book(this.Name, this.Author, this.Rarity)
                {
                    Name = this.Name,
                    Author = this.Author,
                    Rarity = this.Rarity,
                    OverD = this.OverD,
                    Date = this.Date,
                    Sub = null
                };
            }
        }

    }

    public class Subscriber
    {
        public string Name { get; set; } = null;
        public List<Book> Books { get; set; }

        public Subscriber(string name)
        {
            this.Name = name;
        }

        public List<Book> Sub_book(Library l)
        {
            List<Book> my_book = l.List_of_books(this);
            this.Books = my_book;
            return my_book;
        }

        public Book this[string name, string author]
        {
            get
            {
                for (var i = 0; i < Books.Count; i++)
                {
                    if (Books[i].Name == name && Books[i].Author == author)
                    {
                        return (Books[i]);
                    }
                }
                return null;
            }
            set
            {
                for (var i = 0; i < Books.Count; i++)
                {
                    if (Books[i].Name == name && Books[i].Author == author)
                        Books[i] = value;
                }
            }
        }
    }

    public class Library
    {
        public List<Book> storage_book = new List<Book> { };
        internal List<Book> Storage_book { get => storage_book; set => storage_book = value; }

        public void Add_book(Book new_book)
        {
            Console.WriteLine("--- Добавление книги ---");

            Storage_book.Add(new_book);
        }

        public Book Search_book(string name, string author)
        {
            Console.WriteLine("--- Поиск книги ---");

            bool done = false;
            for (var i = 0; i < Storage_book.Count; i++)
            {
                if (Storage_book[i].Name == name && Storage_book[i].Author == author)
                {
                    done = true;
                    return (Storage_book[i]);
                }
            }
            if (done == false)
                throw new Exception($"Этой книги нет в библиотеке!");
            return null;
        }

        public bool Check_overdue(Book book, DateTime current)
        {
            Console.WriteLine("--- Проверка на просроченность ---");

            if (book.IsOverdue(current) == true)
            {
                book.OverD = true;
                return true;
            }
            else
            {
                return false;
            }
        }

        public void Get_out_books(Subscriber man, string name, string author, DateTime current)
        {
            Console.WriteLine("--- Выдача книги ---");
            Book want_book = Search_book(name, author);

            if ((want_book != null) && (want_book.IsBookInLibrary() == true))
            {
                int count = 0;
                int c_rarity = 0;
                int c_overdue = 0;
                foreach (Book b in storage_book)
                {
                    //Console.WriteLine(b.About_book);
                    //Console.WriteLine(b.Sub?.Name);
                    if (b.Sub?.Name == man.Name)
                    {
                        if (Check_overdue(b, current) == true)
                            c_overdue += 1;

                        count += 1;

                        if (b.Rarity == true)
                            c_rarity = 1;
                    }
                }
                //Console.WriteLine(count);
                //Console.WriteLine(c_rarity);
                //Console.WriteLine(c_overdue);
                if (((want_book.Rarity == true) && (c_rarity != 0)) || (count > 5) || (c_overdue != 0))
                    throw new Exception($"У этого жадного абонента на руках либо более 5 книг, либо что-то просрочено!");
                else
                    want_book.GetBookFromLibrary(man, current);
            }
        }

        public void Return_book(string name, string author)
        {
            Console.WriteLine("--- Возврат книги ---");

            Book ret_book = Search_book(name, author);
            ret_book.ReturnBook();
        }

        public List<Book> List_of_books(Subscriber man)
        {
            List<Book> return_storage = new List<Book> { };
            foreach (Book b in storage_book)
            {
                if (b.Sub?.Name == man.Name)
                {
                    Book clone = (Book)b.Clone();
                    return_storage.Add(clone);
                }
            }
            return return_storage;
        }

        public Book this[int index]
        {
            get
            {
                return Storage_book[index];
            }
            set
            {
                Storage_book[index] = value;
            }
        }
    }
    /*
    class Program
    {
        static void Main(string[] args)
        {
            
            DateTime date = DateTime.Today;
            Console.WriteLine(date);

            Book a = new Book("a", "aa", true);
            Book b = new Book("b", "bb", false);
            Book c = new Book("c", "cc", false);
            Book d = new Book("d", "dd", false);
            Book e = new Book("e", "ee", false);
            Book f = new Book("f", "ff", true);
            Book g = new Book("g", "gg", true);
            Book h = new Book("h", "hh", false);

            Subscriber man = new Subscriber("man");

            Library lib = new Library();

            lib.Add_book(a);
            lib.Add_book(b);
            lib.Add_book(c);
            lib.Add_book(d);
            lib.Add_book(e);
            lib.Add_book(f);
            lib.Add_book(g);
            lib.Add_book(h);


            try
            {
                lib.Get_out_books(man, "a", "aa", date);
                lib.Get_out_books(man, "b", "bb", date);
                //lib.Get_out_books(man, "c", "aa", date);
                lib.Get_out_books(man, "d", "dd", date);
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
            }

            var doa = a.About_book;
            Book xy = (Book)a.Clone();
            var res = xy.About_book;

            Console.WriteLine(doa);
            Console.WriteLine(res);

            var pam = lib.List_of_books(man);
            foreach (Book t in pam)
            {
                Console.WriteLine(t.About_book);
            }
            Console.Read();
           
        }
    } */
}
