﻿using System;
using System.Reflection;


namespace TestRunning
{
    class Program
    {
        static void Main(string[] args)
        {
            MethodInfo test;
            Assembly l_dll = Assembly.LoadFrom("LibraryUnitTest.dll");

            Console.WriteLine(l_dll.FullName);
            Console.WriteLine("____");
            Type[] types = l_dll.GetTypes();
            foreach (Type t in types)
            {
                object obj = Activator.CreateInstance(t);
                foreach (MethodInfo method in t.GetMethods())
                {
                    Console.Write("Метод: " + method.ReturnType.Name + " " + method.Name);
                    Console.WriteLine("");
                    test = t.GetMethod(method.Name);
                    try
                    {
                       test.Invoke(obj, null);
                    }
                    catch (Exception ex)
                    {
                        Console.WriteLine("Ошибка! ");
                        Console.WriteLine(ex.Message);
                    } 
                    // t.IsDefined(typeof(TestClass), false); // Не удаётся найти TestClass, TestMethod..
                }
            }
            Console.ReadLine();
        }
    }
}