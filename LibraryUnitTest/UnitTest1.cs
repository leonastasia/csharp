﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace Library
{
    [TestClass]
    public class UnitTest
    {
        /* Добавление несуществующей книги в библиотеку */
        [TestMethod]
        public void Add()
        {
            DateTime date = DateTime.Today;

            Library lib = new Library();

            Book a = new Book("a", "aa", true);
            Book b = new Book("b", "bb", false);
            Book c = new Book("c", "cc", false);

            Subscriber man = new Subscriber("man");

            lib.Add_book(a);
            lib.Add_book(b);
            lib.Add_book(c);

            lib.Get_out_books(man, "a", "aa", date);
            lib.Get_out_books(man, "b", "bb", date);
            try
            {
                lib.Get_out_books(man, "c", "aa", date); // ошибочная строка
            }
            catch(Exception ex)
            {
                Console.WriteLine(ex.Message);
            }
        }

        /* Проверка книги на просроченность */
        [TestMethod]
        public void Overdue()
        {
            try
            {
                DateTime my_date = new DateTime(2016, 2, 1, 8, 30, 52);

                Library lib = new Library();

                Book a = new Book("a", "aa", true);
                Book b = new Book("b", "bb", false);
                Book c = new Book("c", "cc", false);

                Subscriber man = new Subscriber("man");

                lib.Add_book(a);
                lib.Add_book(b);
                lib.Add_book(c);

                lib.Get_out_books(man, "a", "aa", my_date);

                DateTime today = DateTime.Today;

                bool result = a.IsOverdue(today);
                Assert.IsFalse(result, "Overdue!");
            }
            catch(Exception ex)
            {
                Console.WriteLine(ex.Message);
            }
        }

        /* Проверка копирования списка книг, принадлежащих читателю */
        [TestMethod]
        public void Clone()
        {
            DateTime date = DateTime.Today;

            Library lib = new Library();

            Book a = new Book("a", "aa", true);
            Book b = new Book("b", "bb", false);
            Book c = new Book("c", "cc", false);

            Subscriber man = new Subscriber("man");

            lib.Add_book(a);
            lib.Add_book(b);
            lib.Add_book(c);

            lib.Get_out_books(man, "a", "aa", date);
            lib.Get_out_books(man, "b", "bb", date);
            lib.Get_out_books(man, "c", "cc", date);

            var list_man = lib.List_of_books(man);
            Assert.AreEqual(a.Sub.Name, list_man[0].Sub.Name);
            Assert.AreEqual(a.Name, list_man[0].Name);
        }

        /* Попытка выдать 3 редких книги */
        [TestMethod]
        public void Get_out_books()
        {
            try
            {
                DateTime date = DateTime.Today;

                Book a = new Book("a", "aa", true);
                Book b = new Book("b", "bb", false);
                Book c = new Book("c", "cc", true);
                Book d = new Book("d", "dd", true);
                Book e = new Book("e", "ee", false);

                Subscriber man = new Subscriber("man");

                Library lib = new Library();

                lib.Add_book(a);
                lib.Add_book(b);
                lib.Add_book(c);
                lib.Add_book(d);
                lib.Add_book(e);

                lib.Get_out_books(man, "a", "aa", date);
                lib.Get_out_books(man, "b", "bb", date);
                lib.Get_out_books(man, "c", "cc", date);
                lib.Get_out_books(man, "d", "dd", date); // ошибка
                lib.Get_out_books(man, "e", "ee", date);
            }
            catch(Exception ex)
            {
                Console.WriteLine(ex.Message);
            }
        }

        /* Поиск книги по индексатору "имя, автор" у абонента */
        [TestMethod]
        public void Index_sub_search()
        {
            try
            {
                DateTime date = DateTime.Today;

                Book a = new Book("a", "aa", true);
                Book b = new Book("b", "bb", false);
                Book c = new Book("c", "cc", true);;

                Subscriber man = new Subscriber("man");

                Library lib = new Library();

                lib.Add_book(a);
                lib.Add_book(b);
                lib.Add_book(c);


                lib.Get_out_books(man, "a", "aa", date);
                lib.Get_out_books(man, "b", "bb", date);

                man.Sub_book(lib);

                Book d = new Book("d", "dd", true);
                man["a", "aa"] = d;
                Book my = lib.Search_book(man["d", "dd"].Name, man["d", "dd"].Author);
                my.WhenBook();
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
            }
        }

        /* Поиск книги по индексатору у библиотеки */
        [TestMethod]
        public void Index_lib_search()
        {
            Book a = new Book("a", "aa", true);
            Book b = new Book("b", "bb", false);
            Book c = new Book("c", "cc", true); ;

            Library lib = new Library();

            lib.Add_book(a);
            lib.Add_book(b);
            lib.Add_book(c);

            Book d = new Book(lib[0].Name, lib[1].Author, lib[2].Rarity);
            try
            {
                Assert.IsFalse(d.IsBookInLibrary(), "No!");
            }
            catch(Exception ex)
            {
                Console.WriteLine(ex.Message); 
            }
            }
    }
}
